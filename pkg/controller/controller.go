package controller

import (
	"github.com/donseba/go-htmx"
	"github.com/rmn87/gohaml"
)

type RepsonseHeaders map[string]string

type ControllerResponse interface {
	StatusCode() int
	Headers() RepsonseHeaders
	ResponseText() string
}

type ControllerFunc func(RequestInfo, *gohaml.Engine, *htmx.Handler) ControllerResponse

var ctrl map[string]ControllerFunc = map[string]ControllerFunc{}

func Register(loc string, fn ControllerFunc) {
	ctrl[loc] = fn
}

func Controllers() map[string]ControllerFunc {
	return ctrl
}

type controllerResponse struct {
	content    string
	statusCode int
	headers    RepsonseHeaders
}

var _ ControllerResponse = (*controllerResponse)(nil)

func (cr *controllerResponse) StatusCode() int {
	return cr.statusCode
}

func (cr *controllerResponse) ResponseText() string {
	return cr.content
}

func (cr *controllerResponse) Headers() RepsonseHeaders {
	return cr.headers
}

func NewResponse(status int, content string, contentType string) ControllerResponse {
	return &controllerResponse{
		statusCode: status,
		content:    content,
		headers: map[string]string{
			"content-type": contentType,
		},
	}
}

func DefaultController(vars RequestInfo, engine *gohaml.Engine, h *htmx.Handler) ControllerResponse {
	content := engine.Render(vars.Query().I())
	return NewResponse(200, content, "text/html")
}
