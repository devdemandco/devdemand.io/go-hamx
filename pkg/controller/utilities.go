package controller

type MapStringString map[string]string

type MapStringArrString map[string][]string

type RequestInfo interface {
	Headers() MapStringArrString
	Method() string
	Query() MapStringArrString
	RouteVars() MapStringString
}

type requestInfo struct {
	method    string
	headers   map[string][]string
	query     map[string][]string
	routeVars map[string]string
}

var _ RequestInfo = (*requestInfo)(nil) // Compile time check

func NewRequest(method string, routeVars map[string]string, queryVars map[string][]string, headers map[string][]string) RequestInfo {
	r := requestInfo{method, headers, queryVars, routeVars}
	return r
}

func (r requestInfo) Headers() MapStringArrString {
	return r.headers
}

func (r requestInfo) Method() string {
	return r.method
}

func (r requestInfo) Query() MapStringArrString {
	return r.query
}

func (r requestInfo) RouteVars() MapStringString {
	return r.routeVars
}

func (m MapStringString) I() map[string]interface{} {
	v := map[string]interface{}{}

	for key, value := range m {
		v[key] = value
	}

	return v
}

// Assumes only one string value for key
func (m MapStringArrString) Get(k string) string {
	if v := m[k]; len(v) > 0 {
		return v[0]
	}

	return ""
}

// Makes no assumptions of the type for key
func (m MapStringArrString) I() map[string]interface{} {
	v := map[string]interface{}{}

	for key, value := range m {
		v[key] = value
	}

	return v
}
