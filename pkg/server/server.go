package server

import (
	"embed"
	"net/http"

	"github.com/donseba/go-htmx"
	"github.com/gorilla/mux"
	"github.com/rmn87/gohaml"
)

type Server interface {
	Serve() error
}

var _ Server = (*server)(nil)

type Handler func(http.ResponseWriter, *http.Request)

type EngineCallback func() *gohaml.Engine

type server struct {
	r       *mux.Router
	htmx    *htmx.HTMX
	options *serverOptions
}

var defaultServerOptions = serverOptions{
	content:     embed.FS{},
	partialsDir: "partials",
	pagesDir:    "pages",
	apiEndpoint: "api",
	port:        ":8080",
	devMode:     false,
}

func New(options ...ServerOptFunc) Server {
	r := mux.NewRouter()
	opts := defaultServerOptions
	h := htmx.New()
	s := &server{r: r, htmx: h, options: &opts}
	for _, apply := range options {
		apply(s)
	}

	s.generatePartialHandlers(s.options.partialsDir, s.options.devMode)

	return s
}

func (s *server) Serve() error {
	http.Handle("/", s.r)
	http.ListenAndServe(s.options.port, nil)
	return nil
}
