package server

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"

	"github.com/gorilla/mux"
	"github.com/rmn87/gohaml"
	"gitlab.com/devdemandco/devdemand.io/go-hamx/pkg/controller"
)

func (s *server) generatePartialHandlers(dir string, devMode bool) {
	apiTemplates, err := s.options.content.ReadDir(dir)
	if err != nil {
		fmt.Printf("Error when grabbing list of error templates: %s", err)
		return
	}

	for _, template := range apiTemplates {
		if template.IsDir() {
			s.generatePartialHandlers(dir+"/"+template.Name(), devMode)
			continue
		}

		if template.Type().IsRegular() {
			file, err := s.options.content.Open(dir + "/" + template.Name())
			if err != nil {
				fmt.Printf("failed loading file: %s", template.Name())
				continue
			}

			fileBytes, err := io.ReadAll(file)
			if err != nil {
				fmt.Printf("failed reading file: %s", err)
				continue
			}

			engine, err := gohaml.NewEngine(string(fileBytes))
			if err != nil {
				fmt.Printf("failed parsing haml file: %s", err)
				continue
			}

			handler_path := strings.Replace(dir, s.options.partialsDir, s.options.apiEndpoint, 1)
			handler_name := strings.Replace(template.Name(), ".haml", "", 1)
			api_name := handler_path + "/" + handler_name
			fmt.Printf("API: %s\n", api_name)
			if devMode {
				s.r.HandleFunc("/"+api_name, s.generateDynamicHandler(api_name, dir+"/"+template.Name()))
			}
			s.r.HandleFunc("/"+api_name, s.generateHandler(api_name, func() *gohaml.Engine { return engine }))
		}
	}
}

func (s *server) generateHandler(name string, engine EngineCallback) Handler {
	ctrl := controller.Controllers()[name]
	if ctrl == nil {
		ctrl = controller.DefaultController
	}
	return func(w http.ResponseWriter, r *http.Request) {
		h := s.htmx.NewHandler(w, r)
		v := controller.NewRequest(
			r.Method,
			mux.Vars(r),
			r.URL.Query(),
			r.Header,
		)

		resp := ctrl(v, engine(), h)
		for key, value := range resp.Headers() {
			w.Header().Add(key, value)
		}
		w.WriteHeader(resp.StatusCode())
		_, err := w.Write([]byte(resp.ResponseText()))
		if err != nil {
			fmt.Printf("failed writing content to browser: %s", err)
		}
	}
}

func (s *server) generateDynamicHandler(controllerName string, template string) Handler {
	return s.generateHandler(controllerName, func() *gohaml.Engine {
		contents, err := os.ReadFile(template)
		if err != nil {
			fmt.Printf("failed opening template on disk: %s", err)
		}

		err = nil
		if engine, err := gohaml.NewEngine(string(contents)); err != nil {
			panic(err)
		} else {
			return engine
		}

	})
}
