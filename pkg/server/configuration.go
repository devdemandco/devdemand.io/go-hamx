package server

import "embed"

type serverOptions struct {
	content     embed.FS
	partialsDir string
	pagesDir    string
	apiEndpoint string
	port        string
	devMode     bool
}

type ServerOptFunc func(*server)

func WithContent(content embed.FS) ServerOptFunc {
	return func(s *server) {
		s.options.content = content
	}
}

func WithPort(port string) ServerOptFunc {
	return func(s *server) {
		s.options.port = port
	}
}

func WithDevMode(isOn bool) ServerOptFunc {
	return func(s *server) {
		s.options.devMode = isOn
	}
}
