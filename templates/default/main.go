package main

import (
	"embed"
	"flag"

	_ "default/controller"

	"gitlab.com/devdemandco/devdemand.io/go-hamx/pkg/server"
)

//go:embed pages partials static
var content embed.FS
var devMode bool

func init() {
	flag.BoolVar(&devMode, "devMode", false, "Set devMode to allow dynamic recompiling of templates per request")
}

func main() {
	flag.Parse()
	app := server.New(server.WithContent(content), server.WithDevMode(devMode))

	app.Serve()
}
