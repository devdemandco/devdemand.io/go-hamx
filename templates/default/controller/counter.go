package controller

import (
	"github.com/donseba/go-htmx"
	"github.com/rmn87/gohaml"
	ctrl "gitlab.com/devdemandco/devdemand.io/go-hamx/pkg/controller"
)

func init() {
	ctrl.Register("api/counter", main_counter)
	ctrl.Register("api/counter/increment", increment_counter)
}

var inMemoryCounter = 0

func increment_counter(r ctrl.RequestInfo, template *gohaml.Engine, h *htmx.Handler) ctrl.ControllerResponse {
	inMemoryCounter = inMemoryCounter + 1
	return main_counter(r, template, h)
}

func main_counter(r ctrl.RequestInfo, template *gohaml.Engine, h *htmx.Handler) ctrl.ControllerResponse {
	return ctrl.NewResponse(200, template.Render(r.RouteVars().I()), "text/html")
}
