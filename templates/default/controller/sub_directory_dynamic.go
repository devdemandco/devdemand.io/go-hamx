package controller

import (
	"github.com/donseba/go-htmx"
	"github.com/rmn87/gohaml"
	ctrl "gitlab.com/devdemandco/devdemand.io/go-hamx/pkg/controller"
)

func init() {
	ctrl.Register("api/sub-directory/{dynamic}", sub_directory_dynamic_controller)
}

func sub_directory_dynamic_controller(scope ctrl.RequestInfo, template *gohaml.Engine, h *htmx.Handler) ctrl.ControllerResponse {
	return ctrl.NewResponse(200, template.Render(scope.RouteVars().I()), "text/html")
}
