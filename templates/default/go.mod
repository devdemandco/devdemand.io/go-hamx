module default

go 1.21.0

require (
	github.com/donseba/go-htmx v1.0.1
	github.com/rmn87/gohaml v0.0.0-20180130181317-89e8c4e2cad1
	gitlab.com/devdemandco/devdemand.io/go-hamx v0.0.0-20230902150453-c18be1b1d32c
)
