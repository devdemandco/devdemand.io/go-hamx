module gitlab.com/devdemandco/devdemand.io/go-hamx

go 1.21.0

require (
	github.com/donseba/go-htmx v1.0.1
	github.com/gorilla/mux v1.8.0
	github.com/rmn87/gohaml v0.0.0-20180130181317-89e8c4e2cad1
)
