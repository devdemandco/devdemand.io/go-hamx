# Go HAMX

## What is Go Hamx?
Go HAMX stands for Go with haml + htmx. The aim of the project is to stick strictly to go / haml to rapidly build web applications.

## Project Goals
1. No build system - Just go build and run the binary somewhere
2. Opinionated Admin - Similar to laravel, django etc... have a built-in admin backend for database work
3. Other stuff?

## How are pages & partials compiled?
Because go haml libraries have limited to no support for variables, and there is already a great set of templating in go, we use the templ library to process the haml templates. The variables are injected into the haml statically so that haml's only job is to make building html easy.